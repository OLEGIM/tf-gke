variable "name" {
  default = "cluster"  
}

variable "service_name" {
  default = "service"
}
variable "app_name" {
  default = "app"
}

variable "project" {
  default = "final-275021"
}

variable "zone" {
  default = "europe-west2"
}

variable "location" {
  default = "europe-west2-a"
}

variable "initial_node_count" {
  default = 1
}

variable "machine_type" {
  default = "n1-standard-2"  
}
variable "disk_size_gb" {
  default = 10
}
variable "disk_type" {
  default = "pd-standard"
}

variable "network_name" {
  default = "app"
}
