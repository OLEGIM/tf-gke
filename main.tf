resource "google_container_cluster" "default" {
  name = "${var.name}"
  project = "${var.project}"
  description = "Final-Otus"
  location = "${var.location}"

  remove_default_node_pool = true
  initial_node_count = "${var.initial_node_count}"

  network_policy {
    enabled = true
    provider = "CALICO"
  }

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "service" {
  name = "${var.service_name}-node-pool"
  project = "${var.project}"
  location = "${var.location}"
  cluster = "${google_container_cluster.default.name}"
  node_count = 2

  node_config {
    preemptible  = false
    disk_size_gb = "${var.disk_size_gb}"
    disk_type    = "${var.disk_type}"
    machine_type = "${var.machine_type}"

    # taint {
    #   effect = "NO_SCHEDULE"
    #   key    = "node-role"
    #   value  = "service"
    #   }  

    metadata = {
      disable-legacy-endpoints = "true"
    }
      
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
resource "google_container_node_pool" "app" {
  name = "${var.app_name}-node-pool"
  location = "${var.location}"
  cluster = "${google_container_cluster.default.name}"
  node_count = 2

  node_config {
    preemptible  = false
    disk_size_gb = "${var.disk_size_gb}"
    disk_type    = "${var.disk_type}"
    machine_type = "${var.machine_type}"
  
    taint {
      effect = "NO_SCHEDULE"
      key    = "node-role"
      value  = "app"
      }  
  
    # monitoring_service = "disable"
    # logging_service    = "disable"
    
    labels = {
      node_pool = "app"
      }

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      ]
  }
  
}

resource "google_storage_bucket" "robot-storage"{
  name      = "robot-storage"
  project   = "${var.project}"
  location  = "${var.zone}"
}
# resource "google_compute_network" "infra" {
#     name = "${var.network_name}"
#     auto_create_subnetworks = false
# }


# resource "google_compute_subnetwork" "infra" {
#     name = "${var.network_name}"
#     ip_cidr_range = "	10.154.0.0/20"
#     # network = ${google_compute_network.default.self.link}
#     region = "${var.region}"
#     private_ip_google_access = true 
# }