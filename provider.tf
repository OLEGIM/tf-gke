provider "google" {
  credentials = file("service_account.json")
  version = "~> 3.18.0"
  project = "${var.project}"
  }

provider "google-beta" {
  credentials = file("service_account.json")
  version = "~> 3.18.0"
  project = "${var.project}"
}
