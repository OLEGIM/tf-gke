## [Terraform init](https://www.padok.fr/en/blog/kubernetes-google-cloud-terraform-cluster)

```bash
 <project_name> = имя проекта
 <service_account_name> = имя сервисного аккаунта
```

### Разрешим сервисы

```bash
gcloud services enable compute.googleapis.com
gcloud services enable servicenetworking.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable container.googleapis.com
```

### создать сервис аккаунт (create service account)

```bash

gcloud iam service-accounts create terraform-andrew

```

### дать grant permition to some roles (create grant some permission in gcp)

```bash
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/container.admin
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/compute.admin
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/compute.storageAdmin
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/iam.serviceAccountUser
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/resourcemanager.projectIamAdmin
```

### создать и скачать key который позволяет аутентироваться как сервис аккаунт(dnowload accont key for project)

```bash
gcloud iam service-accounts keys create --iam-account my-iam-account@somedomain.com service_account.json
```

## Terraform plan

```bash
terraform plan
```

## Terraform apply

```bash
terraform apply --auto-approve
```
